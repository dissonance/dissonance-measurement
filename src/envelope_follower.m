function [amp_env] = envelope_follower(x)

%amplitude envelope tracker for data x
%Envelope Detection based on Hilbert Transform 

analy=hilbert(x);
env=abs(analy);

%applying moving average filter to further smooth envelope
M = 10;
b = 1/M * ones(1,M);
a = 1;
amp_env = filter(b,a,env);