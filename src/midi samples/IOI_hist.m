%% loop through all midi files, find their spectrogram
% extract vertical lines from the spectrogram and calculate the 
% inter-onset intervals. This metric could be related to complexity
% of polyrhythm

close all, clear all;
midi_list = dir('*.wav');
winSize = 1023;
hopSize = 256;
fftSize = 2048;
nbins = 50;
lin_ls_err = zeros(length(midi_list),1);
midi_labels  = {};

for i = 1:length(midi_list)
    midi_labels{i} = midi_list(i).name(1:end-8);
    [x,fs] = audioread(midi_list(i).name);
    %convert from stereo to mono and truncate to 1s
    xMono = sum(x,2)/size(x,2);
    xMono = xMono(1:fs);
    %get spectrogram characteristics
    [S,F,T] = spectrogram(xMono, blackman(winSize), hopSize, fftSize, fs, 'yaxis');
    %normalise 
    X = abs(S)/max(max(abs(S)));
    %convert to dB
    X = 20*log10(X);
    %plot spectrogram
    figure(i);
    subplot(311);
    imagesc(T,F,X);hold on;
    set(gca,'YDir','normal');
    xlabel('Time in seconds');
    ylabel('Frequency in Hz');
    title(midi_labels{i});
    colormap default;
    colorbar;
    
    %take a look at higher frequencies (>5000Hz)
    ind = find(F > 5000,1);
    Xhigh = X(ind:end,:);
    %this is kind of like filtering based on intensity
    %these limits need to be adjusted
    [freq_1,time_1] = find(Xhigh > -120 & Xhigh < -80);
    %find unique time slices only 
    % The indeces of the same internsity impulses (?)
    time_1_unique = unique(time_1);
    time_1s = T(time_1_unique);
    time_1s_diff = diff(time_1s); 
    
    %plot vertical slices on spectrogram
    figure(i);
    subplot(311);
    for j = 1:length(time_1s)
        plot([time_1s(j), time_1s(j)],[0,fs/2],'r','LineWidth',2);
        hold on;
    end
    hold off;
    subplot(312);
    % NOTE: the histogram shows the distribution of time differences between each pulses
    % the other thing could be done here is to retrieve a periodicity
    % between two interlaid ryhtyms 
    histogram(time_1s_diff,nbins); 
    xlabel('Time difference in seconds');
    title('Histogram of IOI');
    
    %find sorted unique IOI differences
    time_1s_diff_unique = uniquetol(time_1s_diff,20^-6);
    
    %NOTE - if these are all integer multiples of the lowest IOI, then
    %we should see a straight line. If the line has a discontinuity, then 
    %the IOIs are not related by simple integer ratios, which is indicative
    %of a more complex polyrhytm
    
    %plot the sorted unique IOI differences 
    x_axis = 0:length(time_1s_diff_unique)-1;
    if(~isempty(x_axis))
        figure(i);
        subplot(313);
        plot(x_axis,time_1s_diff_unique,'ro');hold on;
        plot(x_axis,time_1s_diff_unique,'b','Linewidth',1.5);
        hold off;
        title('IOI');
    end
    
end

%% Do you see any other patterns in the data? For example, the tritone should 
%be more dissonant than major 3, but both have IOIs in a straight line. Can you
%tell by looking at the IOI histogram which is going to be more dissonant?

