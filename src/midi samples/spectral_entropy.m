%% Calculate the spectral entropy of the signal
clear all;close all;

%% load the audio files
midi_list = dir('*.wav');
winSize = 1023;
hopSize = 256;
fftSize = 2048;
fs = 44100;

H = zeros(1,fs);
I = zeros(length(midi_list),fs/2);
I_diff = zeros(length(midi_list),fs/2);
H_sort = zeros(length(midi_list),fs/2);

for i = 1:length(midi_list)
    
    midi_labels{i} = midi_list(i).name(1:end-8);
    [x,fs] = audioread(midi_list(i).name);
    %convert from stereo to mono and truncate to 1s
    xMono = sum(x,2)/size(x,2);
    xMono = xMono(1:fs);
    %get spectrogram characteristics
    [S,F,T] = spectrogram(xMono, blackman(winSize), hopSize, fftSize, fs, 'yaxis');
    % calculate the power spectrum
    X = fft(xMono,fs);
    pSpec = abs(X).^2/length(X);
    % or
    % pSpec = sum(real(S),2)/sum(sum(real(S),2));
    % probability distribution function
    P = pSpec/sum(pSpec);
    % normalized spectral entrophy function
    for j = 1:length(P)
        H(j) = -1*P(j)*log2(P(j))/log2(length(P));     
    end
    
    % time index
    t = 0:1/fs:1-1/fs;
    
    figure(i);
    subplot(211);
    plot(t,H);
    xlabel('time (seconds)');
    ylabel('Spectral Entrophy');
    title(midi_labels{i});
    
    [H_sort(i,:),I(i,:)] = sort(H(1:fs/2),'descend');
    I_diff(i,1:end-1) = diff(I(i,:))/(fs/2);
    
    
    % built-in spectal entropy function
    [p,fp,tp] = pspectrum(xMono, fs, 'spectrogram');
    
    subplot(212);
    plot(fp,pow2db(sum(p,2)));
    grid on
    xlabel('Frequency (Hz)')
    ylabel('Power Spectrum (dB)')
    % Default Frequency Resolution
    title('Power Spectrum');
    
    % spectral entropy
%     se = pentropcloy(p,fp,tp);
%     
%     subplot(313);
%     pentropy(p,fp,tp);
%     title('Spectral Entropy (built-in function)');

% NOTE: Matlab's built-in spectral entropy function needs 2018a revision. I
% need better internet than my phone's hotspot to download the new version. 
% The function I implemented might have a minor problem. It shows the 
% reflection of peaks and there are only two peaks which does not show ant
% pattern between intervals. 
    
end

