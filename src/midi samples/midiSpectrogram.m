%% spectrogram of the midi samples
clear all; close all;
%% take the spectrogram
path = 'major3rd_C_E.wav';
[x,fs] = audioread(path);

% convert to mono for analysis
xMono = sum(x,2)/size(x,2);
xMono = xMono(1:fs);

% spectrogram
% dry signal
figure (1);
ftgram(xMono,fs,'rir');

% built-in spectrogram
figure (2);
spectrogram(xMono, blackman(1024), 256, 2048, fs, 'yaxis');
title(path);

%% find the spectral entophy