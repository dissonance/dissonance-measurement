function[energy] = teager_energy(x)
%function to calculate teager energy 

L = length(x);
energy = zeros(L,1);

%hmm, not sure about this - maybe use interpolation?
energy(1) = x(1)^2 - x(1)*x(2);
energy(end) = x(end)^2 - x(end-1)*x(end);

for n = 2:L-1
    energy(n) = x(n)^2 - x(n+1)*x(n-1);
end
  
end