%% Generate the sawtooth wave and pass it through the filterbanks
%clear all; 
close all;

%number of octaves should depend on frequency of input signal
nOctaves = 4;
order = 3;
bpo = 3;
nBands = nOctaves * bpo;
ftop = 8000;

fs = 44100;
f1 = 200;
deltaf = 1;
nwin = 200;
Q = 2^(1/2*bpo)/(2^(1/bpo)-1);
winSize = 0.05;

%generate sawtooth wave
x = saw(f1,fs,deltaf,nwin, winSize);

y = zeros(nBands-1,winSize*fs);
energy_teager = zeros(nBands,winSize*fs);
env_tracker = zeros(nBands,winSize*fs);
% note - variance of any sine wave of amplitude 1 is 0.5
variance_teager = zeros(nBands,nwin);
variance_env = zeros(nBands,nwin);

%%Suggestion - replace variance calculation with spectral flatness.
%It seems like variance of flat envelope is the same as variance of
%sinusoidal envelope. Instead a sinusoid will have a low flatness value,
%whereas a flat envelope will have a high flatness value.

fl_teager = zeros(nBands, nwin);
fl_env = zeros(nBands, nwin);

for i=1:nwin
    
    [y, bandCenters] = third_octave_filterbank(x(:,i),fs,order,nBands,ftop,bpo);
    
    for j = 1:nBands
        energy_teager(j,:) = teager_energy(y(j,:));
        env_tracker(j,:)  = envelope_follower(y(j,:));
        variance_teager(j,i) = var(energy_teager(j,:));
        variance_env(j,i) = var(env_tracker(j,:));
        fl_teager(j,i) = geomean(abs(energy_teager(j,:)))/mean(abs(energy_teager(j,:)));
        fl_env(j,i) = geomean(abs(env_tracker(j,:)))/mean(abs(env_tracker(j,:)));
        
        
%         figure (j);
%         subplot(311);
%         plot(y(j,:));
%         %soundsc(repmat(y(j,:),1,20),fs);
%         xlabel('Sample number');
%         ylabel(strcat('Filterbank ', num2str(j),' output'));
%         title(strcat('Band center = ', num2str(bandCenters(j))));
%         subplot(312);
%         plot(energy_teager(j,:));
%         ylabel('Teager energy');
%         subplot(313);
%         plot(env_tracker(j,:));
%         ylabel('Envelope tracker');
%         pause;
         
        
    end    
end

str = {};
for i = 1:nBands
    str{i} = strcat('Band ', num2str(i));
end

df = deltaf:deltaf:nwin*deltaf;

%plot variance in each band
figure;
plot(df,mag2db(variance_teager));
xlabel('Frequency difference (Hz)');
ylabel('variance in dB');
title('Teager energy operator');
legend(str);

figure;
plot(df,mag2db(variance_env));
xlabel('Frequency difference(Hz)');
ylabel('variance in dB');
title('Envelope follower');
legend(str);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%plot spectral flatness in each band
figure;
plot(df,mag2db(fl_teager));
xlabel('Frequency difference (Hz)');
ylabel('Flatness in dB');
title('Teager energy operator');
legend(str);

figure;
plot(df,mag2db(fl_env));
xlabel('Frequency difference(Hz)');
ylabel('Flatness in dB');
title('Envelope follower');
legend(str);

