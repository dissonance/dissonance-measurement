%% Generate the sawtooth wave and pass it through the filterbanks
clear all; 
close all;

%number of octaves should depend on frequency of input signal
nOctaves = 4;
order = 3;
bpo = 3;
nBands = nOctaves * bpo;
ftop = 8000;

fs = 44100;
f1 = 1000;
deltaf = 1;
nwin = 200;
Q = 2^(1/2*bpo)/(2^(1/bpo)-1);
winSize = 0.05;

%generate sawtooth wave
x = saw(f1,fs,deltaf,nwin, winSize);
% concatenated input signal
xc = x(:);

y = zeros(nBands,length(xc));
energy_teager = zeros(nBands,length(xc));
env_tracker = zeros(nBands,length(xc));

[y, bandCenters] = third_octave_filterbank(xc,fs,order,nBands,ftop,bpo);

 for j = 1:nBands
        energy_teager(j,:) = teager_energy(y(j,:));
        env_tracker(j,:)  = envelope_follower(y(j,:));
        
 end

str = {};
for i = 1:nBands
    str{i} = strcat('Band ', num2str(i));
end

dur = length(xc)/fs;
t = 0:1/fs:dur-1/fs;
numPoints = 2000;

figure;
plot(t(1:numPoints),y(:,1:numPoints));
xlabel('time, s');
ylabel('Filterbank output');
grid on;
axis('tight');
legend(str);

figure;
plot(t(1:numPoints),energy_teager(:,1:numPoints));
xlabel('time, s');
ylabel('Teager energy output');
title('Teager energy operator');
legend(str);

figure;
plot(t(1:numPoints),env_tracker(:,1:numPoints));
xlabel('time, s');
ylabel('Envelope tracker');
title('Envelope follower');
legend(str);

%% variance averaged over 2Q cycles
% it would be nice to view the variance on the same time scale

cycleB = round(2*Q*fs./bandCenters);
len = length(energy_teager(1,:));
winNum = floor(len./cycleB);

variance_teager = zeros(nBands,winNum(end));
time_var = zeros(nBands, winNum(end));
time_var_sorted = zeros(nBands, winNum(end));


for i = 1:nBands   
    time_var(i,1:winNum(i)) =(1:winNum(i))*(len/fs/winNum(i));
    start = 1;
    for j=1:winNum(i)
        variance_teager(i,j) = var(energy_teager(i, start:start+cycleB(i)-1));
        start = start + cycleB(i);
    end
    %to get rid of the line joining the first and last points of the plot, the
    %data needs to be sorted.
    [time_var_sorted(i,:), dum] = sort(time_var(i,:));
    variance_teager(i,:) = variance_teager(i,dum);
end
   
figure;
plot(time_var_sorted',variance_teager');
xlabel('Time in seconds');
ylabel('Variance in 2Q Cycles');
legend(str);

%try to plot variance against frequency difference - this is complicated
df = deltaf:deltaf:nwin*deltaf;
time_df = winSize:winSize:nwin*winSize;
df_var = zeros(nBands, winNum(end));

%need to linearly interpolate df to get the right vector length
for i = 1:nBands
    df_var(i,1:winNum(i)) = interp1(time_df,df,time_var(i,1:winNum(i)));  
    %to get rid of the line joining the first and last points of the plot, the
    %data needs to be sorted.
    [df_var(i,:), dum] = sort(df_var(i,:));
end

figure;
plot(df_var',variance_teager');
xlabel('Frequency difference in Hz');
ylabel('Variance in 2Q Cycles');
legend(str);