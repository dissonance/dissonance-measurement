%% Two sawtooth waveform generattion

function [output] = saw(f1, fs, deltaf, nwin, winSize) 
% each colomn of the output is a one window

winSizeSamp = winSize*fs; % window size in samples
df = deltaf:deltaf:nwin*deltaf; % delta f, frequency differnece
dur = winSize*nwin; % duration in seconds
t = 0:1/fs:dur-1/fs; % time vector

% frequencies
f2 = repelem(f1+df,winSizeSamp); % second sawtooth fundemental freq

x1 = sawtooth(2*pi*f1*t);
x2 = sawtooth(2*pi*f2.*t);
y = x1+x2;

output = reshape(y, winSizeSamp, nwin);

% figure(1);
% plot(t(2*winSizeSamp:3*winSizeSamp),y(2*winSizeSamp:3*winSizeSamp));
% hold on;
% %figure(2);
% plot(t(2*winSizeSamp+1:3*winSizeSamp),output(:,3));
% hold off;
end
