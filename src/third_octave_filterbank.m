function [output, bandCenters] = third_octave_filterbank(x,fs,order,nBands,topFreq,bpo)

%implements a third octave filterbank
%output is a matrix (nBands x length(input))   


i = -1:nBands;
bandCenters = topFreq .* 2^(-0.5/bpo) .* 2.^(-(nBands-1-i)/bpo);
%ask Julius about the frequency limits
bandUpperFreq = zeros(1,nBands-1);
bandLowerFreq = zeros(1,nBands-1);
butterb = zeros(nBands,2*order+1);
buttera = zeros(nBands,2*order+1);
N = 4096;
output = zeros(nBands,length(x));

for k = 1:nBands
    bandUpperFreq(k) = sqrt(bandCenters(k+1)*bandCenters(k+2));
    bandLowerFreq(k) = sqrt(bandCenters(k)*bandCenters(k+1));
    [butterb(k,:),buttera(k,:)] = butter(order,[bandLowerFreq(k) bandUpperFreq(k)]/(fs/2));
    [H,f] = freqz(butterb(k,:),buttera(k,:),N,fs);
%     figure(1);
%     semilogx(f,abs(H));hold on;grid on;
%     figure(2);
%     plot(f,abs(H));hold on;grid on;
    output(k,:) = filtfilt(butterb(k,:),buttera(k,:),x);
end

bandCenters = bandCenters(2:end-1);

end