function [am,car,mod] = AM(mod_ind, fm, fc, fs, Ac, dur)

%function to compute an amplitude modulated signal given the modulation
%index and modulation frequency

%time vector
t = 0:1/fs:dur-1/fs;
%carrier signal
car = Ac*sin(2*pi*fc*t);
%modulation signal
mod = sin(2*pi*fm*t);
%AM signal
am = (1+mod_ind*mod).*car;


end