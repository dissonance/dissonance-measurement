close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%FILTERBANK TESTING

%test output of filterbank
f0 = 440;
fs = 44100;
t = 0:1/fs:0.05-1/fs;

x = sawtooth(2*pi*f0*t);
figure(3);
plot(t,x);

nOctaves = 4;
order = 3;
bpo = 3;
nBands = nOctaves * bpo;
%change bands per octave to 5 (Bark band), 7 (ERB)
[y, bandCenters] = third_octave_filterbank(x,44100,order,nBands,8000,bpo);
str = {};

figure(4);
for i = 1:nBands
    str{i} = strcat('Band ', num2str(i));
    plot(t,y(i,:));hold on;grid on;
    pause;
end
legend(str);

%average output of Teager energy integral over two cycles of frequency

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%AMPLITUDE TRACKER TESTING

%generate AM signal
fs = 44100;
mod_ind = 0.5;
fm = 10;
fc = 440;
Ac = 1;
dur = 1;
t = 0:1/fs:dur-1/fs;
[am,car,mod] = AM(mod_ind,fm,fc,fs,Ac,dur);

%generate additive sinusoids very close in frequency
f0 = 440;
delta_f = 10;
add_sin = sin(2*pi*f0*t) + 0.1*sin(2*pi*(f0+delta_f)*t);

%test amplitude tracker on AM
amp_am = envelope_follower(am);
%test amplitude tracker on beating sinusoids
amp_add = envelope_follower(add_sin);

figure(1);
plot(t,add_sin);hold on;
plot(t,amp_env,'r','LineWidth',2);grid on;hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%TEAGER ENERGY FUNCTION TESTING 

%testing the properties of teager energy integral - these should be zero
const = 10*ones(1,length(t));
teager_const = teager_energy(const);
expn = exp(-10*t);
teager_exp = teager_energy(expn);

%test teager energy on simple sine wave
teager_sine = teager_energy(car);
%test teager energy on AM
teager_am = teager_energy(am);
%test teager energy on additive sinusoids
teager_add = teager_energy(add_sin);

%normalize teager energy output
norm_factor = (amp_env*2*pi*(f0/fs)).^2;

figure(2);
subplot(211);
plot(t,teager_am);
subplot(212);
plot(t,norm_factor);


